import React from 'react'
import '../../styles/ProfileCard.css'
import { BsFillCircleFill } from "react-icons/bs";
function ProfileCard() {
    return (
        <div className="container is-fluid profileCardsContainer">
        <div className="columns is-vcentered is-multiline">
          <div className="column is-12-mobile is-4-desktop is-12-tablet mt-5 is-offset-2-desktop">
            <div className="profileCardsBox py-6 px-4"
            data-aos="fade-down-right"
            data-aos-once="true"
            data-aos-easing="ease-in-out"
            data-aos-duration="1000">
              <span className="cardHeader">
                <span className="circleBox p-3">
                  <BsFillCircleFill className="circle" />
                </span>
                <h4 className="cardHeaderText has-text-left is-size-5-desktop is-size-6-tablet is-size-6-mobile">
                  Follower/Following of @any_User
                </h4>
              </span>
              <p className="profileCardText mt-5 has-text-left">
                Lorem Ipsum something is there. Something more will come here. Oh
                yes it will come.
              </p>
              <div className="has-text-centered profileCardsBtnBox mt-5">
                <button className=" profileCardsBtn">SAMPLE DATA</button>
              </div>
            </div>
          </div>
          <div className="column is-12-mobile is-4-desktop is-12-tablet mt-5">
            <div className="profileCardsBox py-6 px-4"
            data-aos="fade-down-left"
            data-aos-once="true"
            data-aos-easing="ease-in-out"
            data-aos-duration="1000">
              <span className="cardHeader ">
                <span className="circleBox p-3">
                  <BsFillCircleFill className="circle1" />
                </span>
                <h4 className="cardHeaderText has-text-left is-size-5-desktop is-size-6-tablet is-size-6-mobile">
                  Liker/Commenter of any post 
                </h4>
              </span>
              <p className="profileCardText mt-5 has-text-left">
                Lorem Ipsum something is there. Something more will come here. Oh
                yes it will come.
              </p>
              <div className="has-text-centered profileCardsBtnBox mt-5">
                <button className=" profileCardsBtn1">SAMPLE DATA</button>
              </div>
            </div>
          </div>
          <div className="column is-12-mobile is-4-desktop is-12-tablet mt-5 is-offset-2-desktop">
            <div className="profileCardsBox py-6 px-4"
            data-aos="fade-up-right"
            data-aos-once="true"
            data-aos-easing="ease-in-out"
            data-aos-duration="1000">
              <span className="cardHeader ">
                <span className="circleBox p-3">
                  <BsFillCircleFill className="circle2" />
                </span>
                <h4 className="cardHeaderText has-text-left is-size-5-desktop is-size-6-tablet is-size-6-mobile">
                  People Posting Specific #Hashtags
                </h4>
              </span>
              <p className="profileCardText mt-5 has-text-left">
                Lorem Ipsum something is there. Something more will come here. Oh
                yes it will come.
              </p>
              <div className="has-text-centered profileCardsBtnBox mt-5">
                <button className=" profileCardsBtn2">SAMPLE DATA</button>
              </div>
            </div>
          </div>
          <div className="column is-12-mobile is-4-desktop is-12-tablet mt-5">
            <div
              className="profileCardsBox py-6 px-4"
              data-aos="fade-up-left"
              data-aos-once="true"
              data-aos-easing="ease-in-out"
              data-aos-duration="1000"
            >
              <span className="cardHeader ">
                <span className="circleBox p-3">
                  <BsFillCircleFill className="circle3" />
                </span>
                <h4 className="cardHeaderText has-text-left is-size-5-desktop is-size-6-tablet is-size-6-mobile">
                  People Posting at Specific Locations
                </h4>
              </span>
              <p className="profileCardText mt-5 has-text-left">
                Lorem Ipsum something is there. Something more will come here. Oh
                yes it will come.
              </p>
              <div className="has-text-centered profileCardsBtnBox mt-5">
                <button className="profileCardsBtn3">SAMPLE DATA</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
}

export default ProfileCard
