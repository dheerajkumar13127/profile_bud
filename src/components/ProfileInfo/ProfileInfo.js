import React from 'react'
import '../../styles/ProfileInfo.css'
function ProfileInfo() {
    return (
        <div className="container is-fluid mt-3 profileInfoContainer p-4">
        <div className="columns is-vcentered">
          <div className="column is-12">
            <h1 className="has-text-centered has-text-light profileInfoHeader ">
              We Extract Data & Leads From Instagram
            </h1>
            <p className="has-text-centered mt-5 profileInfoAbout">
              Our agency does all the hard work and <br /> gives you the clean
              targeted data in Sheets
            </p>
            <div className="has-text-centered profileInfoBtnBox">
              <button className="button profileInfoBtn is-large">GET LEADS</button>
            </div>
          </div>
        </div>
      </div>
    )
}

export default ProfileInfo
