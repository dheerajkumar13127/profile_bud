import React, { useState } from "react"
import "../../styles/Navbar.css"
import { Link } from "gatsby"

export default function Navbar() {
  const [active, setActive] = useState(false)
  const [navBarActiveClass, setNavBarActiveClass] = useState("")

  function toggleHamburger() {
    setActive(!active)

    active ? setNavBarActiveClass("is-active") : setNavBarActiveClass("")
  }

  return (
    <div>
      <nav
        className="navbar  p-5"
        role="navigation"
        aria-label="main-navigation"
      >
        <div className="container is-fluid nav-container ">
          <div className="navbar-brand ">
          <a href="/" className="logo-link">
            <img src="/images/logo.svg" alt="profile" className="navLogo" />
          </a>
          <a
            className={`header-nav is-vcentered navbar-burger burger ${navBarActiveClass}`}
            data-target="navMenu"
            onClick={() => toggleHamburger()}
            // style={{
            //   margin: "1em 1em 1em 1em",
            // }}
          >
            <span />
            <span />
            <span />
          </a>
          </div>
        
      

          <div id="navMenu" className={`navbar-menu ${navBarActiveClass}`}>
            <div className="navbar-end is-vcentered is-size-5-tablet  has-text-centered">
              <Link
                className="navbar-item has-text-weight-bold nav-link is-size-4-desktop is-size-6-tablet is-size-6-mobile mt-2"
                to="/"
              >
                USPs
              </Link>
               <Link
                className="navbar-item has-text-weight-bold nav-link is-size-4-desktop is-size-6-tablet is-size-6-mobile mt-2"
                to="/"
              >
                PRICING
              </Link>
              <Link
                className="nav-item-cart navbar-item  nav-link mt-2"
                to="/"
              >
                <img src="/images/cart.svg" alt="" className="navbar-cart" />
              </Link>
            </div>
          </div>
        </div>
      </nav>
    </div>
  )
}
{
  /* <nav className="navbar navbar-expand-lg p-4">
<div className="container-fluid">
  <a
    className="navbar-brand text-dark  navHeaderText d-flex align-items-center"
    href="/"
  >
    <img src="/images/logo.svg" alt="logo" className="navLogo" />
  </a>
  <button
    className="navbar-toggler"
    type="button"
    data-bs-toggle="collapse" 
    data-bs-target="#navbarNav"
    aria-controls="navbarNav"
    aria-expanded="false"
    aria-label="Toggle navigation"
  >
    <GiHamburgerMenu className="text-light" />
  </button>
  <div
    className="collapse navbar-collapse  justify-content-end"
    id="navbarNav"
  >
    <ul className="navbar-nav p-2  nav d-flex align-items-center">
      <li className="nav-item mt-2">
        <a className=" nav-link" href="/">
          USPs
        </a>
      </li>
      <li className="nav-item mt-2">
        <a className="nav-link" href="/">
          PRICING
        </a>
      </li>
      <li className="nav-item nav-item-cart p-3 mt-2">
        <img src="/images/cart.svg" alt="" className="navbar-cart" />
      </li>
    </ul>
  </div>
</div>
</nav> */
}
