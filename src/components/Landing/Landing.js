import React from 'react'
import '../../styles/Landing.css'
import ProfileAccount from '../ProfileAccount.js/ProfileAccount'
import ProfileCard from '../ProfileCard/ProfileCard'
import ProfileInfo from '../ProfileInfo/ProfileInfo'
function Landing() {
    return (
        <div className="landingContainer p-3">
            <ProfileInfo/>
            <ProfileCard/>
            <ProfileAccount/>
        </div>
    )
}

export default Landing
