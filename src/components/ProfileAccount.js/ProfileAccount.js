import React from 'react'
import '../../styles/ProfileAccount.css'
function ProfileAccount() {
    return (
        <div className="container is-fluid profileAccountsContainer">
      <div className="columns is-vcentered">
        <div
          className="column is-12-mobile is-5-desktop is-5-tablet is-offset-2 mt-3"
          data-aos="fade-right"
          data-aos-once="true"
          data-aos-easing="ease-in-out"
          data-aos-duration="1000"
        >
          <div className="profileAccountsBox">
            <h4 className="has-text-centered has-text-light profileAccountsHeader is-size-3-desktop is-size-5-tablet is-size-5-mobile">
              NUMBER OF ACCOUNTS
            </h4>
            <div className="has-text-centered mt-5">
              <form>
                <div className="input-group mb-3 mt-4">
                  <input
                    type="text"
                    className="form-control input"
                    placeholder="ENTER NUMBER HERE"
                    aria-describedby="addon-wrapping"
                    required
                  />
                </div>
              </form>
            </div>
            <div className="mt-5">
              <h1 className="has-text-centered has-text-light profileAccountsAmount is-size-3-desktop  is-size-3-tablet is-size-3-mobile">
                $0.00
              </h1>
            </div>
          </div>
        </div>
        <div
          className="column is-12-mobile is-4-desktop is-4-tablet mt-4"
          data-aos="fade-left"
          data-aos-once="true"
          data-aos-delay="200"
          data-aos-duration="1000"
        >
          <h5 className="has-text-left profileAccountsTryText mt-1">
            TRY IT FREE
          </h5>
          <h5 className="has-text-left profileAccountsPayText mt-4">
            Pay As You Go Rates
          </h5>
          <p className="has-text-left profileAccountsSubText">
            Emails Price per Email
          </p>
          <p className="has-text-left profileAccountsSubText">
            up to 10,000 $0.008
          </p>
          <p className="has-text-left profileAccountsSubText">
            up to 100,000 $0.005
          </p>
          <p className="has-text-left profileAccountsSubText">
            up to 250,000 $0.004
          </p>
          <p className="has-text-left profileAccountsSubText">
            up to 1,000,000 $0.003
          </p>
          <h5 className="has-text-left profileAccountsPayText mt-4">
            Enterprise Packages
          </h5>
          <p className="has-text-left profileAccountsSubText">over 1,000,000</p>
          <h5 className="has-text-left profileAccountContactText mt-2">
            Contact Us
          </h5>
          <h5 className="has-text-left profileAccountsLearnText mt-3">
            LEARN MORE ABOUT ENTERPRISE
          </h5>
        </div>
      </div>
    </div>
    )
}

export default ProfileAccount
