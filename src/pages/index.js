import React from "react"
import styles from '../styles/common.module.css'
import Navbar from "../components/Navbar/Navbar"
import Landing from "../components/Landing/Landing"

export default () => (
  <div className={styles.back}> 
    <Navbar/>
    <Landing/>
  </div>

)
  