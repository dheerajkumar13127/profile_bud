import React from "react"
import { Link } from "gatsby"

const NotFoundPage = () => (
  <div>
    <div className="text-center">
      <img src="https://cdn.rawgit.com/ahmedhosna95/upload/1731955f/sad404.svg" alt="404" />
      <br />
      <span>404</span>
      <p className="p-a">Page not Found</p>
      <p className="p-b text-center">
        Oops! The Page you are looking for has been removed or relocated or does
        not exits.
      </p>
      <Link to="/" className="back">
        Go To Home
      </Link>
    </div>
  </div>
)

export default NotFoundPage
